﻿using UnityEngine.UI;

namespace TDDExample.Tests.HeartTest.Infrastructure
{
    public class HeartBuilder: TestDataBuilder<Heart>
    {
        private Image _image;
        
        public HeartBuilder(Image image)
        {
            _image = image;
        }

        public HeartBuilder(): this(An.Image())
        {
            //Debug.Log("HeartBuilder()");
        }

        public HeartBuilder With(Image image)
        {
            //Debug.Log("Image is replaced by _image");
            _image = image;
            return this;
        }

        public override Heart Build()
        {
            //Debug.Log("heart builded by Build()");
            return new Heart(_image);
        }

    }
}
