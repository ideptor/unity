﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TDDExample.Tests.HeartTest.Infrastructure
{
    public class HeartContainerBuilder : TestDataBuilder<HeartContainer>
    {
        private List<Heart> _hearts;

        public HeartContainerBuilder(List<Heart> hearts)
        {
            _hearts = hearts;
        }

        public HeartContainerBuilder() : this(new List<Heart> { A.Heart()})
        {

        }

        public HeartContainerBuilder With(Heart heart, params Heart[] hearts)
        {
            _hearts = new List<Heart> { heart };
            foreach (var heart_elem in hearts)
            {
                _hearts.Add(heart_elem);
            }
            return this;
        }

        public override HeartContainer Build()
        {
            return new HeartContainer(_hearts);
        }
    }
}
