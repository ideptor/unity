﻿using System;
using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace TDDExample.Tests.HeartTest
{
    public class PlayerTests
    {
        public class TheCurrentHealthProperty
        {
            [Test]
            public void Health_Defaults_To_0()
            {
                var player = new Player(0);

                Assert.AreEqual(0, player.CurrentHealth);
            }

            [Test]
            public void Throws_Exception_When_Current_Health_Is_Less_Than_0()
            {
                //Assert.Throws<ArgumentOutOfRangeException>(() => new Player(-1));
                Assert.Throws<ArgumentOutOfRangeException>(() => new Player(-1));
            }

            [Test]
            public void Throws_Exception_When_Current_Health_Is_Greater_Than_Maximum_Health()
            {
                Assert.Throws<ArgumentOutOfRangeException>(() => new Player(2, 1));
            }
        }

        public class TheHealthMethod
        {
            [Test]
            public void _0_Does_Nothig()
            {
                var player = new Player(0);

                player.Heal(0);

                Assert.AreEqual(0, player.CurrentHealth);
            }

            [Test]
            public void _1_Increments_Current_Health()
            {
                var Player = new Player(0);

                Player.Heal(1);

                Assert.AreEqual(1, Player.CurrentHealth);
            }

            [Test]
            public void Overhealling_Is_Ignored()
            {
                var player = new Player(0, 1);

                player.Heal(2);

                Assert.AreEqual(1, player.CurrentHealth);
            }

            [Test]
            public void _1_Decrements_Current_Health()
            {
                var player = new Player(1);

                player.Damage(1);

                Assert.AreEqual(0, player.CurrentHealth);
            }

            [Test]
            public void Overkill_Is_Ignored()
            {
                var player = new Player(1);

                player.Damage(2);

                Assert.AreEqual(0, player.CurrentHealth);
            }
        }
    }
}
