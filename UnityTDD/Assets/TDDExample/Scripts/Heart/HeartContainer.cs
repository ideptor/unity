﻿using System;
using System.Collections.Generic;

public class HeartContainer
{
    private readonly List<Heart> _hearts;

    public HeartContainer(List<Heart> hearts)
    {
        this._hearts = hearts;
    }

    public void Replenish(int heartPieces)
    {

        foreach (var heart in _hearts)
        {
            var toReplenish = (heartPieces < heart.EmptyHeartPieces)
                ? heartPieces
                : heart.EmptyHeartPieces;

            heartPieces -= heart.EmptyHeartPieces;
            heart.Replenish(toReplenish);
            if (heartPieces <= 0) break;
        }

    }

    public void Deplete(int heartPieces)
    {
        _hearts.Reverse();
        foreach (var heart in _hearts)
        {
            var toDeplete = (heartPieces < heart.FilledHeartPieces)
                ? heartPieces
                : heart.FilledHeartPieces;

            heartPieces -= heart.FilledHeartPieces;
            heart.Deplete(toDeplete);
            if (heartPieces <= 0) break;
        }
        _hearts.Reverse();
    }
}