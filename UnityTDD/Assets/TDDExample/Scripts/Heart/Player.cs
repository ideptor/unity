﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


public class Player
{
    public int CurrentHealth { get; private set; }
    public int MaximumHealth { get; private set; }

    public Player(int currentHealth, int maximumHealth = 12)
    {
        if (currentHealth < 0) throw new ArgumentOutOfRangeException(
            "currentHealth should not be less then 0; but "+currentHealth);

        if (currentHealth > maximumHealth) throw new ArgumentOutOfRangeException(
            $"currentHealth should not larger then {maximumHealth}; but {currentHealth}");

        CurrentHealth = currentHealth;
        MaximumHealth = maximumHealth;
    }

    public void Heal(int amount)
    {
        CurrentHealth = Math.Min(CurrentHealth + amount, MaximumHealth);
    }

    public void Damage(int amount)
    {
        CurrentHealth = Math.Max(CurrentHealth - amount, 0);
    }
}

