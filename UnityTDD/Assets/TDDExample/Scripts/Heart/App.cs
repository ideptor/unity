﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class App : MonoBehaviour
{
    [SerializeField] private Image _image;
    [SerializeField] private int _amount;
    [SerializeField] private List<Image> _images;

    private HeartContainer _heartContainer;

    private void Start()
    {
        _heartContainer = new HeartContainer(
            _images.Select(image => new Heart(image)).ToList());
    }

    
    // Update is called once per frame
    void Update()
    {

               
        if(Input.GetKeyDown(KeyCode.UpArrow))
        {
            Debug.Log("Replenish");
            _heartContainer.Replenish(_amount);
        }
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            Debug.Log("Deplete");
            _heartContainer.Deplete(_amount);
        }
    }

}
